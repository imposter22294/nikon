from rest_framework.authentication import BasicAuthentication

# from webapp.models import Feat_Safety, Feat_Comfort, Feat_Misc, Feat_Warranty, \
#     Spec_Capacity, Spec_Dimensions, Spec_Engine, Spec_Suspension, Spec_Brake


def validate(request,requestSerializer):
    serializedRequest = requestSerializer(data = request.DATA)
    if not serializedRequest.is_valid():
        return None
    requestTypeObject = serializedRequest.create(request.DATA)
    print requestTypeObject
    return requestTypeObject
    
def authenticate(request,requestSerializer):
    serializedRequest = requestSerializer(data = request.DATA)
    requestTypeObject = serializedRequest.restore_object(request.DATA)
    ba = BasicAuthentication()
    ba.authenticate_credentials(requestTypeObject.username,requestTypeObject.password)
    return requestTypeObject

def validateAndAuthenticate(request,requestSerializer):
    
    if validate(request, requestSerializer) == None :
        return None
    requestTypeObject = authenticate(request,requestSerializer)
    return requestTypeObject

def sendResponse(response,responseSerailizer,multiple=True):
    response = responseSerailizer(response,many=multiple)
    return response.data
