from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from searchengine.apis.getSearchResults import getSearchResults
# from searchengine.apis.getUserComment import getUserComment


#from webapp.api import testApiOne
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cars.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'api/',testApiOne),
    url(r'getSearchResults/',getSearchResults),
    # url(r'getUserComment/',getUserComment),

)

urlpatterns = format_suffix_patterns(urlpatterns)