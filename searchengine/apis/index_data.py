import json
from elasticsearch import Elasticsearch


# Loading Data from Json silw
jdata = open("nikon.json").read()
data = json.loads(jdata)

#instantiate ES
es = Elasticsearch()

#Creating a index
es.indices.create(index='cams', ignore=400)

#Indexing all products and making them searchable
for product in data:
    es.create(index='cams', doc_type='product', body=product)

# Refreshing Index
es.indices.refresh(index='cams')

# title = "Nikon Coolpix  P530 Point & Shoot Camera"
# # A sample query (Query DSl)
# q1 = {
#     "match" : {
#         "title" : title
#     }
# }

# nik = es.search(index='cams', doc_type='', body={"query":q1})

# #print(nik)
# #print(nik['hits']['hits'])

# results = nik['hits']['hits']

# for r in results:
# 	print r['_score'], r['_source']['title']