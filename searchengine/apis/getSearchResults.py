import json
from elasticsearch import Elasticsearch

from rest_framework import serializers
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


# from searchengine.common.serializerUtilities import StringListField
from searchengine.common.utilities import validate, sendResponse #,get_features_dictionary

class request_type_object_class(object):
    def __init__ (self,full_text):
        self.full_text = full_text

class requestSerializer(serializers.Serializer):
    full_text = serializers.CharField()
    
    def create(self, attrs):
        return request_type_object_class(**attrs)   
    
    def update(self,attrs,instance=None):
        if instance is not None :
            instance.full_text = attrs['full_text']
            return instance
        return request_type_object_class(**attrs)

class responseSerializer(serializers.Serializer):
    source = serializers.DictField()
    
    def create(self, attrs):
        return response_type_object_class(**attrs)   
    
    def update(self,attrs,instance=None):
        if instance is not None :
            instance.source = attrs['source']
            return instance
        return response_type_object_class(**attrs)

class response_type_object_class(object):
    def __init__(self,source):
        self.source = source


def get_response_type_objects(request_type_object): 
    #create the query and return all the results
    search_term = request_type_object.full_text
    # Loading Data from Json
    q1 = {
    "match" : {
        "title" : search_term
        }
    }
    #instantiate ES
    es = Elasticsearch()
    # Refreshing Index
    es.indices.refresh()
    result_list = []
    nik = es.search(body={"query":q1})
    results = nik['hits']['hits']
    for r in results:
        returnTypeObject = response_type_object_class(source = r['_source'])
        result_list.append(returnTypeObject)

    return result_list




@api_view(['POST'])
def getSearchResults(request):
    request_type_object = validate(request, requestSerializer)
    if request_type_object == None :
        return Response(status=status.HTTP_400_BAD_REQUEST)
    response_typeobject_list = get_response_type_objects(request_type_object)
    
    response = sendResponse(response_typeobject_list ,responseSerializer, multiple = True)
    if response == -1 :
        return Response(status=status.HTTP_417_EXPECTATION_FAILED)
    return Response(data = response,status=status.HTTP_200_OK)