from django.db import models
from django.contrib.auth.models import User



class Manufacturer(models.Model):
    mid = models.CharField(max_length=25, primary_key=True)
    name = models.CharField(max_length=50)
    logo = models.URLField(max_length = 2000, null=True)
    def __unicode__(self):
        return self.name

